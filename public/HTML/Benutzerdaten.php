<!DOCTYPE HTML>
<HTML>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" type="text/css" herf="../CSS/Styless.css" /> -->
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Tablet.css">
    <title>Personaldaten</title>
</head>

<body class="page-body">
    <header class="page-header">
        <div class="header-title">Personaldaten</div>
    </header>
    <?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>
    <div class="page-content">
        <form action="/HTML/Abwiesenheit.php" class="form-container">
            <div>
                <div class="form-input">
                    <label for="name">Name</label><br>
                    <input type="text" id="name" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="vorname">Vorname</label><br>
                    <input type="text" id="vorname" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="bezeichnung">Bezeichnung</label><br>
                    <input type="text" id="bezeichnung" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="abteilung">Abteilung</label><br>
                    <input type="text" id="abteilung" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="anfang">Anfang</label><br>
                    <input type="date" id="anfang" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="ende">Ende</label><br>
                    <input type="date" id="ende" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="firma">Firma</label><br>
                    <input type="text" id="firma" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="ausbilder">Ausbilder*in</label><br>
                    <input type="text" id="ausbilder" class="input-text" />
                </div>
                <div class="form-input">
                    <input type="submit" value="Weiter" class="form-submit" />
                </div>
            </div>
        </form>
    </div>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</HTML>