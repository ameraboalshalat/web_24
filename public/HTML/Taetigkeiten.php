<!DOCTYPE HTML>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Tablet.css">
    <title>Tätigkeiten</title>
</head>

<body class="page-body">
    <header class="page-header">
        <div class="header-title">
            Tätigkeiten
        </div>
    </header>
    <?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>
    <div class="page-content">
        <form class="form-container" action="/index.php">
            <div>
                <div class="form-input">
                    <label for="bezeichnung">Bezeichnung</label><br>
                    <input class="input-text" type="text" id="bezeichnung" />
                </div>
                <div class="form-input">
                    <label for="prioritaet">Priorität</label><br>
                    <input class="input-text" type="number" id="prioritaet" />
                </div>
                <div class="form-input">
                    <input type="radio" name="active" required> Aktiv
                    <input type="radio" name="active" required> Inaktiv
                </div>
                <div>
                    <h4>Gültigkeit</h4>
                </div>
                <div class="form-input">
                    <label for="anfang">Anfang</label><br>
                    <input class="input-text" type="date" id="anfang" />
                </div>
                <div class="form-input">
                    <label for="ende">Ende</label><br>
                    <input class="input-text" type="date" id="ende" />
                </div>
                <div class="form-input">
                    <label for="firma">Firma</label><br>
                    <input class="input-text" type="text" id="firma" />
                </div>
            </div>
            <div>
                <label for="ausbilder">Ausbilder*in</label><br>
                <input class="input-text" type="text" id="ausbilder" />
            </div>
            <div class="form-row">
                <input class="form-submit" type="submit" value="Speichern" />
            </div>
        </form>
    </div>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</html>