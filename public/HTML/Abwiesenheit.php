<!DOCTYPE HTML>
<HTML>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Tablet.css">
    <title>Abwesenheit</title>
</head>

<body class="page-body">
    <header class="page-header">
        <div class="header-title">
            Abwesenheit
        </div>
    </header>
    <?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>
    <div class="page-content">
        <form action="/HTML/Berufsschule.php" class="form-container">
            <div class="form-row">
                <div class="form-input">
                    <label for="bezeichnung">Bezeichnung</label><br>
                    <input type="text" id="bezeichnung" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="beginn">Beginn</label><br>
                    <input type="date" id="beginn" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="ende">Ende</label><br>
                    <input type="date" id="ende" class="input-text" />
                </div>
            </div>
            <p></p>
            <div class="table-container">
                <div class="table-row table-header ">
                    <div class="table-cell">Bezeichnung</div>
                    <div class="table-cell">Beginn</div>
                    <div class="table-cell">Ende</div>
                </div>
                <div class="table-row">
                    <div class="table-cell">Krank</div>
                    <div class="table-cell">02.03.2024</div>
                    <div class="table-cell">08.03.2024</div>
                </div>
                <div class="table-row">
                    <div class="table-cell">Urlaub</div>
                    <div class="table-cell">01.04.2024</div>
                    <div class="table-cell">11.04.2024</div>
                </div>
            </div>
            <div>
                <input type="submit" value="Weiter" class="form-submit" />
            </div>
        </form>
    </div>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</HTML>