<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" type="text/css" herf="../CSS/Styless.css" /> -->
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Modile.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Tablet.css">
    <link rel="stylesheet" media="print" href="../CSS/PDF_print.css">
    <title>Berichtsheft</title>
</head>

<body>
<?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>

    <header>
        <table border="1" class="Berichtsheft-header-table">
            <tr>
                <td>Name:</td>
                <td><?= $firstname?></td>
                <td>Vorname:</td>
                <td><?= $lastname?></td>
                <td>Ausbildungaabteilung:</td>
                <td><?= $trainingDepartment?></td>
            </tr>
            <tr>
                <td>Nr. Ausbildungswoche</td>
                <td><?= $trainingWeek?></td>
                <td>Von Jahr</td>
                <td><?= $year?></td>
                <td>Ausbildungsjahr</td>
                <td><?= $trainingYear?></td>
            </tr>
        </table>
    </header>
    <h1>Ausbildungsnachweis</h1>
    <table border="1">
        <tr>
            <td>Betriebliche Tätigkeiten</td>
        </tr>
        <tr>
            <td>
            <?= $opertionalAktivity?>
            </td>
        </tr>
        <tr>
            <td>Unterweisungen, Lehrgespräche, betrieblicher Unterricht, sonstige Schulungen</td>
        </tr>
        <tr>
            <td>
                <?= $instruction?>            
            </td>
        </tr>
        <tr>
            <td>Berufsschule (Unterrichtsthemen)</td>
        </tr>
        <tr>
            <td>
                <?= $schoolSubject?>
            </td>
        </tr>
    </table>
    <div>
        <span>Durch die nachfolgenden Unterschriften wird die Richtigkeit und Vollständigkeit der obigen Angaben
            bestätigt.</span>
    </div>
    <table border="1">
        <tr>
            <td>Datum:<?= $date?></td>
            <td>Datum:<?= $date?></td>
            <td>Datum:<?= $date?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Auszubildende/-r</td>
            <td>Ausbilder/-in</td>
            <td>Gesetzliche/-r Vertreter/-in</td>
        </tr>
    </table>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</html>