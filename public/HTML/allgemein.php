<!DOCTYPE HTML>
<HTML>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Tablet.css">
    <link rel="stylesheet" media="print" href="../CSS/PDF_print.css">

    <title>Alle Seiten</title>
</head>

<body class="page-body">
<?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>
    <section class="page-section">
        <div class="page-content">
            <header class="page-header">
                <div class="header-title">
                    Personaldaten
                </div>
            </header>
            <form action="/HTML/Benutzerdaten.php" class="form-container">
                <div class="form-row">
                    <label for="name">Name</label><input type="text" id="name" class="form-input" />
                    <label for="vorname">Vorname</label><input type="text" id="vorname" class="form-input" />
                </div>
                <div class="form-row">
                    <label for="bezeichnung">Bezeichnung</label><input type="text" id="bezeichnung"
                        class="form-input" />
                    <label for="abteilung">Abteilung</label><input type="text" id="abteilung" class="form-input" />
                </div>
                <div class="form-row">
                    <label for="anfang">Anfang</label><input type="date" id="anfang" class="form-input" />
                    <label for="ende">Ende</label><input type="date" id="ende" class="form-input" />
                </div>
                <div class="form-row">
                    <label for="firma">Firma</label><input type="text" id="firma" class="form-input" />
                    <label for="ausbilder">Ausbilder*in</label><input type="text" id="ausbilder" class="form-input" />
                </div>
            </form>
        </div>
    </section>

    <!-- Seite 2: Berufsschule -->
    <section class="page-section">
        <div class="page-content">
            <header class="page-header">
                <div class="header-title">
                    Berufsschule
                </div>
            </header>
            <form action="Taetigkeiten.html" class="form-container">
                <div class="form-row">
                    <div class="form-input">
                        <label for="unterrichtsthemen">Unterrichtsthemen</label><br>
                        <input type="text" id="unterrichtsthemen" class="input-text" />
                    </div>
                    <div class="form-input">
                        <label for="beginn">Beginn</label><br>
                        <input type="date" id="beginn" class="input-date" />
                    </div>
                    <div class="form-input">
                        <label for="ende">Ende</label><br>
                        <input type="date" id="ende" class="input-date" />
                    </div>
                </div>
                <div class="form-checkboxes">
                    <label>Wochentage:</label><br>
                    <input type="checkbox" id="montag" name="montag" /><label for="montag">Montag</label>
                    <input type="checkbox" id="dienstag" name="dienstag" /><label for="dienstag">Dienstag</label>
                    <input type="checkbox" id="mittwoch" name="mittwoch" /><label for="mittwoch">Mittwoch</label>
                    <input type="checkbox" id="donnerstag" name="donnerstag" /><label
                        for="donnerstag">Donnerstag</label>
                    <input type="checkbox" id="freitag" name="freitag" /><label for="freitag">Freitag</label>
                    <input type="checkbox" id="samstag" name="samstag" /><label for="samstag">Samstag</label>
                </div>
                <div class="table-container">
                    <div class="table-row table-header">
                        <div class="table-cell">Unterrichtsthemen</div>
                        <div class="table-cell">Beginn</div>
                        <div class="table-cell">Ende</div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">Mathe</div>
                        <div class="table-cell">02.03.2024</div>
                        <div class="table-cell">02.03.2024</div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">HPK</div>
                        <div class="table-cell">01.04.2024</div>
                        <div class="table-cell">01.04.2024</div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">PSE</div>
                        <div class="table-cell">02.03.2024</div>
                        <div class="table-cell">02.03.2024</div>
                    </div>
                    <div class="table-row">
                        <div class="table-cell">OOS</div>
                        <div class="table-cell">01.04.2024</div>
                        <div class="table-cell">01.04.2024</div>
                    </div>
                </div>
            </form>
        </div>
    </section>

    <!-- Seite 3: Abwesenheit -->
    <section class="page-section">
        <div class="page-content">
            <header class="page-header">
                <div class="header-title">
                    Abwesenheit
                </div>
            </header>
            <form action="Berufsschule.html" class="form-container">
                <div class="form-row">
                    <div>
                        <label for="bezeichnung">Bezeichnung</label><input type="text" id="bezeichnung"
                            class="form-input" />
                    </div>
                    <div>
                        <label for="beginn">Beginn</label><input type="date" id="beginn" class="form-input" />
                        <label for="ende">Ende</label><input type="date" id="ende" class="form-input" />
                    </div>
                </div>
                <table class="form-table">
                    <thead>
                        <tr>
                            <th>Bezeichnung</th>
                            <th>Beginn</th>
                            <th>Ende</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Krank</td>
                            <td>02.03.2024</td>
                            <td>08.03.2024</td>
                        </tr>
                        <tr>
                            <td>Urlaub</td>
                            <td>01.04.2024</td>
                            <td>11.04.2024</td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </section>

    <!-- Seite 4: Tätigkeiten -->
    <section class="page-section">
        <div class="page-content">
            <header class="page-header">
                <div class="header-title">
                    Tätigkeiten
                </div>
            </header>
            <form class="form-container" action="/public/HTML/Berichtsheft.html">
                <div class="form-row">
                    <label for="bezeichnung">Bezeichnung</label>
                    <input class="form-input" type="text" id="bezeichnung" />
                    <input type="radio" name="active" required> Aktiv
                    <input type="radio" name="active" required> Inaktiv
                    <label for="prioritaet">Priorität</label>
                    <input class="form-input" type="number" id="prioritaet" />
                </div>
                <div class="form-row">
                    <h4>Gültigkeit</h4>
                    <label for="anfang">Anfang</label>
                    <input class="form-input" type="date" id="anfang" />
                    <label for="ende">Ende</label>
                    <input class="form-input" type="date" id="ende" />
                </div>
                <div class="form-row">
                    <label for="firma">Firma</label>
                    <input class="form-input" type="text" id="firma" />
                    <label for="ausbilder">Ausbilder*in</label>
                    <input class="form-input" type="text" id="ausbilder" />
                </div>
            </form>
        </div>
    </section>
    <section class="page-section">
        <header class="page-header">
            <h2>Weitere Inhalte</h2>
        </header>
        <div class="page-content">
            <p>
                Hier könnte weiterer Inhalt stehen.
            </p>
        </div>
    </section>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</HTML>