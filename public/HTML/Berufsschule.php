<!DOCTYPE HTML>
<HTML>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="stylesheet" type="text/css" herf="../CSS/Styless.css" /> -->
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="../CSS/Styles_Tablet.css">
    <title>Berufsschule</title>
</head>

<body class="page-body">
    <header class="page-header">
        <div class="header-title">
            Berufsschule
        </div>
    </header>
    <?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>
    <div class="page-content">
        <form action="/HTML/Taetigkeiten.php" class="form-container">
            <div class="form-row">
                <div class="form-input">
                    <label for="unterrichtsthemen">Unterrichtsthemen</label><br>
                    <input type="text" id="unterrichtsthemen" class="input-text" />
                </div>
                <div class="form-input">
                    <label for="beginn">Beginn</label><br>
                    <input type="date" id="beginn" class="input-date" />
                </div>
                <div class="form-input">
                    <label for="ende">Ende</label><br>
                    <input type="date" id="ende" class="input-date" />
                </div>
            </div>
            <p></p>
            <div class="form-checkboxes">
                <label>Wochentage:</label><br>
                <input type="checkbox" id="montag" name="montag" /><label for="montag">Montag</label>
                <input type="checkbox" id="dienstag" name="dienstag" /><label for="dienstag">Dienstag</label>
                <input type="checkbox" id="mittwoch" name="mittwoch" /><label for="mittwoch">Mittwoch</label>
                <input type="checkbox" id="donnerstag" name="donnerstag" /><label for="donnerstag">Donnerstag</label>
                <input type="checkbox" id="freitag" name="freitag" /><label for="freitag">Freitag</label>
                <input type="checkbox" id="samstag" name="samstag" /><label for="samstag">Samstag</label>
            </div>
            <p></p>
            <div class="table-container">
                <div class="table-row table-header">
                    <div class="table-cell">Unterrichtsthemen</div>
                    <div class="table-cell">Beginn</div>
                    <div class="table-cell">Ende</div>
                </div>
                <div class="table-row">
                    <div class="table-cell">Mathe</div>
                    <div class="table-cell">02.03.2024</div>
                    <div class="table-cell">02.03.2024</div>
                </div>
                <div class="table-row">
                    <div class="table-cell">HPK</div>
                    <div class="table-cell">01.04.2024</div>
                    <div class="table-cell">01.04.2024</div>
                </div>
                <div class="table-row">
                    <div class="table-cell">PSE</div>
                    <div class="table-cell">02.03.2024</div>
                    <div class="table-cell">02.03.2024</div>
                </div>
                <div class="table-row">
                    <div class="table-cell">OOS</div>
                    <div class="table-cell">01.04.2024</div>
                    <div class="table-cell">01.04.2024</div>
                </div>
            </div>
            <div class="form-row">
                <input type="submit" value="Weiter" class="form-submit" />
            </div>
        </form>
    </div>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</HTML>