<?php
echo "Ausbildungsberechtigungsgenerator"
?>

<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ausbildungsberechtigungsgenerator</title>
    <link rel="stylesheet" type="text/css" href="/CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="/CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="/CSS/Styles_Tablet.css">
</head>

<body class="page-body">
    <header class="page-header">
        <h1>Ausbildungsberechtigungsgenerator</h1>
    </header>
    <?php include_once('/home/amer/Webentwicklung/php_app/HTML/Navigation.php'); ?>

    <main class="page-main">
        <section class="page section">
            <h2 class="section-title">Willkommen beim Ausbildungsberechtigungsgenerator!</h2>
            <p>Hier können Sie Ihre Ausbildungsberechtigung erstellen.</p>
        </section>
        <section class="page-section">
            <h2 class="section-title">Anleitung</h2>
            <p>Füllen Sie das Formular auf der Startseite aus und wählen Sie die entsprechenden Optionen für Ihre
                Ausbildung.</p>
        </section>
    </main>
    <?php include_once('/home/amer/Webentwicklung/PHP_App/HTML/Footer.php'); ?>
</body>

</html>
