<!DOCTYPE html>
<html lang="de">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="CSS/Styles_Desktop.css">
    <link rel="stylesheet" type="text/css" href="CSS/Styles_Mobile.css">
    <link rel="stylesheet" type="text/css" href="CSS/Styles_Tablet.css">
</head>
<body>
    <header class="page-header">
        <h1>Ausbildungsberechtigungsgenerator Login</h1>
    </header>
    <div class="page-container">
        <?php
            if (isset($_GET["error"])) {
                echo "<p class='error-message'>Falscher Benutzername oder Passwort". $_GET['error'] ."</p>" ;
            }   
        ?>
        <form action="login.php" method="post" class="form-container">
            <div class="form-row">
                <label for="username">Benutzername</label><br>
                <input type="text" id="username" name="username" class="input-text" />
            </div>
            <div class="form-row">
                <label for="password">Passwort</label><br>
                <input type="password" id="password" name="password" class="input-text" />
            </div>
            <div class="form-row">
                <button type="submit" class="form-submit">Anmelden</button>
            </div>
        </form> 
    </div>

</body>

</html>

